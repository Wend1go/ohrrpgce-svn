This is a minimal OHRRPGCE game player binary for Linux.
It was built for 32-bit Intel architecture, and is mainly
intended for packaging Linux distributions of OHRRPGCE games
on non-Linux platforms like Windows and Mac OS X.
