------------------------------------------------------------
 O.H.R.RPG.C.E                   (WIP version)
------------------------------------------------------------
Official Hamster Republic RPG Construction Engine
for Mac OS X

---
INSTALLATION

Copy OHRRPGCE-Game.app and OHRRPGCE-Custom.app to your
Applications folder.

The "import" folder contains sample importable music,
sounds, fonts and palettes. You can copy it to somewhere
in your home folder.

The "Vikings of Midgard" folder contains the sample game.
Copy it anywhere in your home folder.

---
HELP

While editing your game with OHRRPGCE-Custom, you can press F1
or Cmd-Shift-? at any time to get help about the current screen.

Visit http://rpg.hamsterrepublic.com/ohrrpgce/Main_Page
for more help.

---

Please see WHATSNEW.TXT for revision information.

---
DISTRIBUTION

The OHRRPGCE is free software under the terms of the GPL
Please read LICENSE.txt for more information. Visit
http://HamsterRepublic.com/ohrrpgce/source.php to download
the source code.

Any .RPG files you create are yours to distribute as you please.
You may freely distribute the OHRRPGCE-Game player, with your
.RPG files provided that you include a copy of LICENSE-binary.txt
so that people will know where to get the source code.

If you have questions, read the FAQ at
http://hamsterrepublic.com/ohrrpgce/F.A.Q.html

---

Report bugs at: http://HamsterRepublic.com/ohrrpgce/buglist.php

---
This program is free software; you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the
Free Software Foundation; either version 2 of the License, or (at your
option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

See LICENSE.txt for the full text of the GPL

---

(EXTERNAL UTILITIES)
madplay
  Copyright (C) 2000-2004 Robert Leslie & Underbit Technologies, Inc.
OggEnc
  Copyright (C) 2000-2005 Michael Smith <msmith@xiph.org>

---

Spiffy, Spiffy! Enjoy the editor.

                                            James Paige
                                            Hamster Republic Productions
                                            http://HamsterRepublic.com/
