'OHRRPGCE - Custom+Game common battle system code
'(C) Copyright 1997-2005 James Paige and Hamster Republic Productions
'Please read LICENSE.txt for GNU GPL details and disclaimer of liability

#IFNDEF BCOMMON_BI
#DEFINE BCOMMON_BI

DECLARE FUNCTION equip_elemental_merge(values() as single, byval formula as integer) as single

#ENDIF