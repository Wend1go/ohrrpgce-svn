'OHRRPGCE CUSTOM - The Menus Editor (not to be confused with menu slices editors)
'(C) Copyright 1997-2017 James Paige and Hamster Republic Productions
'Please read LICENSE.txt for GPL License details and disclaimer of liability

#include "config.bi"
#include "allmodex.bi"
#include "common.bi"
#include "customsubs.bi"
#include "cglobals.bi"
#include "scrconst.bi"
#include "sliceedit.bi"
#include "custom.bi"

'--Local SUBs
DECLARE SUB update_menu_editor_menu(byval record as integer, edmenu as MenuDef, menu as MenuDef)
DECLARE SUB update_detail_menu(detail as MenuDef, menudata as MenuDef, mi as MenuDefItem)
DECLARE SUB menu_editor_keys (state as MenuState, mstate as MenuState, menudata as MenuDef, byref record as integer, menu_set as MenuSet)
DECLARE SUB menu_editor_menu_keys (mstate as MenuState, dstate as MenuState, menudata as MenuDef, byval record as integer)
DECLARE SUB menu_editor_detail_keys(dstate as MenuState, mstate as MenuState, detail as MenuDef, mi as MenuDefItem)
DECLARE SUB edit_menu_bits (menu as MenuDef)
DECLARE SUB edit_menu_item_bits (mi as MenuDefItem)
DECLARE SUB reposition_menu (menu as MenuDef, mstate as MenuState)
DECLARE SUB reposition_anchor (menu as MenuDef, mstate as MenuState)


SUB menu_editor ()

DIM menu_set as MenuSet
menu_set.menufile = workingdir & SLASH & "menus.bin"
menu_set.itemfile = workingdir & SLASH & "menuitem.bin"

DIM record as integer = 0

DIM state as MenuState 'top level
state.active = YES
state.need_update = YES
DIM mstate as MenuState 'menu
mstate.active = NO
mstate.need_update = YES
DIM dstate as MenuState 'detail state
dstate.active = NO

DIM edmenu as MenuDef
ClearMenuData edmenu
WITH edmenu
 .textalign = alignLeft
 .alignhoriz = alignLeft
 .alignvert = alignTop
 .anchorhoriz = alignLeft
 .anchorvert = alignTop
 .boxstyle = 3
 .translucent = YES
 .min_chars = 38
END WITH
DIM menudata as MenuDef
LoadMenuData menu_set, menudata, record
DIM detail as MenuDef
ClearMenuData detail
WITH detail
 .textalign = alignLeft
 .anchorhoriz = alignLeft
 .anchorvert = alignBottom
 .offset.x = -152
 .offset.y = 92
 .min_chars = 36
END WITH

DIM box_preview as string = ""

setkeys YES
DO
 setwait 55
 setkeys YES
 
 IF state.active = NO THEN EXIT DO
 IF mstate.active = YES THEN
  menu_editor_menu_keys mstate, dstate, menudata, record
 ELSEIF dstate.active = YES THEN
  menu_editor_detail_keys dstate, mstate, detail, *menudata.items[mstate.pt]
 ELSE
  menu_editor_keys state, mstate, menudata, record, menu_set
 END IF
 
 IF state.need_update THEN
  state.need_update = NO
  update_menu_editor_menu record, edmenu, menudata
  init_menu_state state, edmenu
  init_menu_state mstate, menudata
 END IF
 IF mstate.need_update THEN
  mstate.need_update = NO
  init_menu_state mstate, menudata
 END IF
 IF dstate.need_update THEN
  dstate.need_update = NO
  update_detail_menu detail, menudata, *menudata.items[mstate.pt]
  init_menu_state dstate, detail
  WITH *menudata.items[mstate.pt]
   IF .t = mtypeTextBox THEN
    box_preview = textbox_preview_line(.sub_t)
   END IF
  END WITH
 END IF
 
 clearpage dpage
 IF NOT mstate.active THEN draw_menu menudata, mstate, dpage
 IF NOT mstate.active AND NOT dstate.active THEN draw_menu edmenu, state, dpage
 IF mstate.active THEN
  draw_menu menudata, mstate, dpage
  edgeprint "ENTER to edit, Shift+Arrows to re-order", 0, pBottom, uilook(uiDisabledItem), dpage
  IF record = 0 THEN
   edgeprint "CTRL+R to reload default", 0, pBottom - 10, uilook(uiDisabledItem), dpage
  END IF
 END IF
 IF dstate.active THEN
  draw_menu detail, dstate, dpage
  IF menudata.items[mstate.pt]->t = 3 THEN '--textbox
   edgeprint box_preview, 0, pBottom, uilook(uiText), dpage
  END IF
 END IF
 
 SWAP vpage, dpage
 setvispage vpage
 dowait
LOOP
SaveMenuData menu_set, menudata, record
ClearMenuData edmenu
ClearMenuData menudata
ClearMenuData detail

END SUB

SUB menu_editor_keys (state as MenuState, mstate as MenuState, menudata as MenuDef, byref record as integer, menu_set as MenuSet)
 IF keyval(scESC) > 1 THEN state.active = NO
 IF keyval(scF1) > 1 THEN show_help "menu_editor_main"
 
 usemenu state
 
 SELECT CASE state.pt
  CASE 0
   IF enter_space_click(state) THEN
    state.active = NO
   END IF
  CASE 1
   DIM saverecord as integer = record
   IF intgrabber_with_addset(record, 0, gen(genMaxMenu), 32767, "menu") THEN
    IF record > gen(genMaxMenu) THEN gen(genMaxMenu) = record
    SaveMenuData menu_set, menudata, saverecord
    LoadMenuData menu_set, menudata, record
    state.need_update = YES
    mstate.need_update = YES
   END IF
  CASE 2
   IF strgrabber(menudata.name, 20) THEN state.need_update = YES
  CASE 3
   IF enter_space_click(state) THEN
    mstate.active = YES
    mstate.need_update = YES
    menudata.edit_mode = YES
    append_menu_item menudata, "[NEW MENU ITEM]"
   END IF
  CASE 4
   IF intgrabber(menudata.boxstyle, 0, 14) THEN state.need_update = YES
  CASE 5
   IF intgrabber(menudata.textcolor, 0, 255) THEN state.need_update = YES
   IF enter_space_click(state) THEN
    menudata.textcolor = color_browser_256(menudata.textcolor)
    state.need_update = YES
   END IF
  CASE 6
   IF intgrabber(menudata.maxrows, 0, 20) THEN state.need_update = YES
  CASE 7
   IF enter_space_click(state) THEN
    edit_menu_bits menudata
    state.need_update = YES
   END IF
  CASE 8
   IF enter_space_click(state) THEN
    reposition_menu menudata, mstate
   END IF
  CASE 9
   IF enter_space_click(state) THEN
    reposition_anchor menudata, mstate
   END IF
  CASE 10 ' text align
   IF intgrabber(menudata.textalign, alignLeft, alignRight) THEN state.need_update = YES
  CASE 11 ' Minimum width in chars
   IF intgrabber(menudata.min_chars, 0, 38) THEN state.need_update = YES
  CASE 12 ' Maximum width in chars
   IF intgrabber(menudata.max_chars, 0, 38) THEN state.need_update = YES
  CASE 13 ' border size
   IF intgrabber(menudata.bordersize, -100, 100) THEN state.need_update = YES
  CASE 14 ' item spacing
   IF intgrabber(menudata.itemspacing, -10, 100) THEN state.need_update = YES
  CASE 15: ' on-close script
   IF enter_space_click(state) THEN
    scriptbrowse menudata.on_close, plottrigger, "menu on-close plotscript"
    state.need_update = YES
   END IF
   IF scrintgrabber(menudata.on_close, 0, 0, scLeft, scRight, 1, plottrigger) THEN state.need_update = YES
  CASE 16: ' esc menu
   IF zintgrabber(menudata.esc_menu, -1, gen(genMaxMenu)) THEN state.need_update = YES
 END SELECT
END SUB

SUB menu_editor_menu_keys (mstate as MenuState, dstate as MenuState, menudata as MenuDef, byval record as integer)
 DIM i as integer
 DIM elem as integer

 IF keyval(scESC) > 1 THEN
  mstate.active = NO
  menudata.edit_mode = NO
  mstate.need_update = YES
  'remove [NEW MENU ITEM]
  remove_menu_item menudata, menudata.last
  EXIT SUB
 END IF
 IF keyval(scF1) > 1 THEN show_help "menu_editor_items"

 usemenu mstate
 IF mstate.pt >= 0 AND mstate.pt < menudata.numitems THEN
 WITH *menudata.items[mstate.pt]
  IF NOT (menudata.edit_mode = YES AND .trueorder.next = NULL) THEN  'not the last item, "NEW MENU ITEM"
   strgrabber .caption, 38
   IF keyval(scEnter) > 1 THEN '--Enter
    mstate.active = NO
    dstate.active = YES
    dstate.need_update = YES
   END IF
   IF keyval(scDelete) > 1 THEN '-- Delete
    IF yesno("Delete this menu item?", NO) THEN
     remove_menu_item menudata, mstate.pt
     mstate.need_update = YES
    END IF
   END IF
   IF keyval(scLeftShift) > 0 OR keyval(scRightShift) > 0 THEN '--holding Shift
    IF keyval(scUp) > 1 AND mstate.pt < mstate.last - 1 THEN ' just went up
     'NOTE: Cursor will have already moved because of usemenu call above
     swap_menu_items menudata, mstate.pt, menudata, mstate.pt + 1
     mstate.need_update = YES
    END IF
    IF keyval(scDown) > 1 AND mstate.pt > mstate.first THEN ' just went down
     'NOTE: Cursor will have already moved because of usemenu call above
     swap_menu_items menudata, mstate.pt, menudata, mstate.pt - 1
     mstate.need_update = YES
    END IF
   END IF
  ELSE
   IF menudata.edit_mode = YES THEN
    'Selecting the item that appends new items
    IF enter_space_click(mstate) THEN
     menudata.last->caption = ""
     append_menu_item menudata, "[NEW MENU ITEM]"
     mstate.active = NO
     mstate.need_update = YES
     dstate.active = YES
     dstate.need_update = YES
    END IF
   END IF
  END IF
 END WITH
 END IF' above block only runs with a valid mstate.pt

 IF record = 0 THEN
  IF keyval(scCtrl) > 0 AND keyval(scR) > 1 THEN
   IF yesno("Reload the default main menu?") THEN
    ClearMenuData menudata
    create_default_menu menudata
    append_menu_item menudata, "[NEW MENU ITEM]"
    mstate.need_update = YES
   END IF
  END IF
 END IF
 
END SUB

SUB menu_editor_detail_keys(dstate as MenuState, mstate as MenuState, detail as MenuDef, mi as MenuDefItem)
 DIM max as integer

 IF keyval(scESC) > 1 THEN
  dstate.active = NO
  mstate.active = YES
  EXIT SUB
 END IF
 IF keyval(scF1) > 1 THEN show_help "menu_editor_item_details"

 usemenu dstate

 DIM editaction as integer = detail.items[dstate.pt]->t
 SELECT CASE editaction
  CASE 0
   IF enter_space_click(dstate) THEN
    dstate.active = NO
    mstate.active = YES
    EXIT SUB
   END IF
  CASE 1: 'caption
   IF strgrabber(mi.caption, 38) THEN
    dstate.need_update = YES
   END IF
  CASE 2: 'type
   IF intgrabber(mi.t, 0, mtypeLAST) THEN
    mi.sub_t = 0
    dstate.need_update = YES
   END IF
  CASE 3: 'subtype
   SELECT CASE mi.t
    CASE mtypeCaption:
     max = 1
    CASE mtypeSpecial
     max = spLAST
    CASE mtypeMenu
     max = gen(genMaxMenu)
    CASE mtypeTextBox
     max = gen(genMaxTextBox)
   END SELECT
   IF mi.t = mtypeScript THEN
    IF scrintgrabber(mi.sub_t, 0, 0, scLeft, scRight, 1, plottrigger) THEN dstate.need_update = YES
    IF enter_space_click(dstate) THEN
     scriptbrowse mi.sub_t, plottrigger, "Menu Item Script"
     dstate.need_update = YES
    END IF
   ELSE
    IF intgrabber(mi.sub_t, 0, max) THEN dstate.need_update = YES
   END IF
  CASE 4: 'conditional tag1
   IF tag_grabber(mi.tag1) THEN dstate.need_update = YES
  CASE 5: 'conditional tag2
   IF tag_grabber(mi.tag2) THEN dstate.need_update = YES
  CASE 6: 'set tag
   IF tag_grabber(mi.settag, , , NO) THEN dstate.need_update = YES
  CASE 7: 'toggle tag
   IF tag_grabber(mi.togtag, 0, , NO) THEN dstate.need_update = YES
  CASE 8: ' bitsets
   IF enter_space_click(dstate) THEN
    edit_menu_item_bits mi
    dstate.need_update = YES
   END IF
  CASE 9 TO 11: 'extra data
   IF intgrabber(mi.extra(editaction - 9), -32768, 32767) THEN dstate.need_update = YES
 END SELECT

END SUB

SUB update_menu_editor_menu(byval record as integer, edmenu as MenuDef, menu as MenuDef)
 DIM cap as string
 DeleteMenuItems edmenu
 
 append_menu_item edmenu, "Previous Menu"
 
 cap = "Menu " & record
 IF record = 0 THEN cap = cap & " (MAIN MENU)"
 append_menu_item edmenu, cap
 
 append_menu_item edmenu, "Name: " & menu.name
 append_menu_item edmenu, "Edit Items..."
 append_menu_item edmenu, "Box Style: " & menu.boxstyle
 append_menu_item edmenu, "Text color: " & zero_default(menu.textcolor)
 append_menu_item edmenu, "Max rows to display: " & zero_default(menu.maxrows)
 append_menu_item edmenu, "Edit Bitsets..."
 append_menu_item edmenu, "Reposition menu..."
 append_menu_item edmenu, "Change Anchor Point..."
 append_menu_item edmenu, "Text Align: " & HorizCaptions(menu.textalign)
 append_menu_item edmenu, "Minimum width: " & zero_default(menu.min_chars, "Automatic")
 append_menu_item edmenu, "Maximum width: " & zero_default(menu.max_chars, "None")
 append_menu_item edmenu, "Border size: " & zero_default(menu.bordersize)
 append_menu_item edmenu, "Item spacing: " & zero_default(menu.itemspacing)
 append_menu_item edmenu, "On-close script: " & scriptname(menu.on_close)
 IF menu.esc_menu = 0 THEN
  cap = "just closes this menu"
 ELSE
  cap = "switch to menu " & menu.esc_menu - 1 & " " & getmenuname(menu.esc_menu - 1)
 END IF
 IF menu.no_close THEN cap = "disabled by bitset"
 append_menu_item edmenu, "Cancel button: " & cap
END SUB

SUB update_detail_menu(detail as MenuDef, menudata as MenuDef, mi as MenuDefItem)
 DIM i as integer
 DIM cap as string
 DIM index as integer
 DeleteMenuItems detail

 ' Set .t of each menu item to indicate what menu_editor_detail_keys should do

 append_menu_item detail, "Go Back", 0
 
 cap = mi.caption
 IF LEN(cap) = 0 THEN cap = "[DEFAULT]"
 append_menu_item detail, "Caption: " & cap, 1
 
 append_menu_item(detail, "Type", 2)
 WITH *detail.last
  SELECT CASE mi.t
   CASE mtypeCaption
    .caption = "Type: " & mi.t & " Caption"
   CASE mtypeSpecial
    .caption = "Type: " & mi.t & " Special screen"
   CASE mtypeMenu
    .caption = "Type: " & mi.t & " Go to Menu"
   CASE mtypeTextBox
    .caption = "Type: " & mi.t & " Show text box"
   CASE mtypeScript
    .caption = "Type: " & mi.t & " Run script"
  END SELECT
 END WITH
 
 append_menu_item(detail, "Subtype: " & mi.sub_t, 3)
 WITH *detail.last
  SELECT CASE mi.t
   CASE mtypeCaption
    SELECT CASE mi.sub_t
     CASE 0: .caption = .caption & " Selectable"
     CASE 1: .caption = .caption & " Not Selectable"
    END SELECT
   CASE mtypeSpecial
    .caption = .caption & " " & get_special_menu_caption(mi.sub_t)
   CASE mtypeMenu
    .caption = .caption & " " & getmenuname(mi.sub_t)
   CASE mtypeScript
    .caption = "Subtype: " & scriptname(mi.sub_t)
    IF mi.sub_t THEN
     ' Indicate which script arguments are passed
     ' (It's safe to call append_menu_item, because WITH saves a reference to *detail.last)
     DIM argsinfo as string = "Args: "
     IF menudata.allow_gameplay THEN
      '0 is passed instead of the menu item handle if it would be invalid
      argsinfo &= IIF(mi.close_if_selected, "0, ", "handle, ")
     ELSE
      'Sadly, for back-compatibility, leave out the handle instead of passing zero.
     END IF
     argsinfo &= "extra0, extra1, extra2"
     append_menu_item(detail, argsinfo, -1)   'type: does nothing
     detail.last->disabled = YES
     detail.last->unselectable = YES  'Does nothing, yet
    END IF
   CASE ELSE
    .caption = "Subtype: " & mi.sub_t
  END SELECT
  .caption &= get_menu_item_editing_annotation(mi)
 END WITH
 
 append_menu_item detail, tag_condition_caption(mi.tag1, "Enable if tag", "Always"), 4
 append_menu_item detail, tag_condition_caption(mi.tag2, " and also tag", "Always"), 5
 append_menu_item detail, tag_set_caption(mi.settag, "Set tag"), 6
 append_menu_item detail, tag_toggle_caption(mi.togtag), 7
 append_menu_item detail, "Edit Bitsets...", 8
 FOR i = 0 TO 2
  append_menu_item detail, "Extra data " & i & ": " & mi.extra(i), 9 + i
 NEXT i
END SUB


SUB edit_menu_bits (menu as MenuDef)
 DIM bitname(9) as string
 DIM bits(0) as integer
 
 bitname(0) = "Translucent box"
 bitname(1) = "Never show scrollbar"
 bitname(2) = "Allow gameplay & scripts"
 bitname(3) = "Suspend player even if gameplay allowed"
 bitname(4) = "No box"
 bitname(5) = "Disable cancel button"
 bitname(6) = "No player control of menu"
 bitname(7) = "Prevent main menu activation"
 bitname(8) = "Advance text box when menu closes"
 bitname(9) = "Highlight selection background"

 MenuBitsToArray menu, bits()
 editbitset bits(), 0, UBOUND(bitname), bitname(), "menu_editor_bitsets"
 MenuBitsFromArray menu, bits()  
END SUB

SUB edit_menu_item_bits (mi as MenuDefItem)
 DIM bitname(2) as string
 DIM bits(0) as integer
 
 bitname(0) = "Hide if disabled"
 bitname(1) = "Close menu if selected"
 bitname(2) = "Don't run on-close script"

 MenuItemBitsToArray mi, bits()
 editbitset bits(), 0, UBOUND(bitname), bitname(), "menu_editor_item_bitsets"
 MenuItemBitsFromArray mi, bits()  
END SUB

SUB reposition_menu (menu as MenuDef, mstate as MenuState)
 DIM shift as integer

 setkeys
 DO
  setwait 55
  setkeys
 
  IF keyval(scESC) > 1 THEN EXIT DO
  IF keyval(scF1) > 1 THEN show_help "reposition_menu"
  
  shift = ABS(keyval(scLeftShift) > 0 OR keyval(scRightShift) > 0)
  WITH menu.offset
   IF keyval(scUp) > 1 THEN .y -= 1 + 9 * shift
   IF keyval(scDown) > 1 THEN .y += 1 + 9 * shift
   IF keyval(scLeft) > 1 THEN .x -= 1 + 9 * shift
   IF keyval(scRight) > 1 THEN .x += 1 + 9 * shift
  END WITH
 
  clearpage dpage
  draw_menu menu, mstate, dpage
  edgeprint "Offset=" & menu.offset, 0, 0, uilook(uiDisabledItem), dpage
  edgeprint "Arrows to re-position, ESC to exit", 0, pBottom, uilook(uiDisabledItem), dpage
  
  SWAP vpage, dpage
  setvispage vpage
  dowait
 LOOP
END SUB

SUB reposition_anchor (menu as MenuDef, mstate as MenuState)
 DIM tog as integer = 0
 DIM x as integer
 DIM y as integer
 setkeys
 DO
  setwait 55
  setkeys
  tog = tog XOR 1
 
  IF keyval(scESC) > 1 THEN EXIT DO
  IF keyval(scF1) > 1 THEN show_help "reposition_anchor"
  
  WITH menu
   IF keyval(scUp) > 1 THEN .anchorvert = bound(.anchorvert - 1, alignTop, alignBottom)
   IF keyval(scDown) > 1 THEN .anchorvert = bound(.anchorvert + 1, alignTop, alignBottom)
   IF keyval(scLeft) > 1 THEN .anchorhoriz = bound(.anchorhoriz - 1, alignLeft, alignRight)
   IF keyval(scRight) > 1 THEN .anchorhoriz = bound(.anchorhoriz + 1, alignLeft, alignRight)
  END WITH
 
  clearpage dpage
  draw_menu menu, mstate, dpage
  WITH menu
   x = .rect.x - 2 + anchor_point(.anchorhoriz, .rect.wide)
   y = .rect.y - 2 + anchor_point(.anchorvert, .rect.high)
   rectangle x, y, 5, 5, 2 + tog, dpage 
  END WITH
  edgeprint "Arrows to re-position, ESC to exit", 0, pBottom, uilook(uiDisabledItem), dpage
  
  SWAP vpage, dpage
  setvispage vpage
  dowait
 LOOP
END SUB
